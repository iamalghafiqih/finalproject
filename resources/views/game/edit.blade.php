@extends('layouts.master')

@section('judul')

    Halaman Edit Data Game

@endsection 

@section('content')

<form action="/game/{{$game->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" value="{{$game->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Gameplay</label>
      <input type="text" name="gameplay" value="{{$game->gameplay}}" class="form-control">
    </div>
    @error('gameplay')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Developer</label>
        <input type="text" name="developer" value="{{$game->developer}}" class="form-control">
      </div>
      @error('developer')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group">
        <label>Year</label>
        <input type="text" name="year" value="{{$game->year}}" class="form-control">
    </div>
    @error('year')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary ">Submit</button>
    
  </form>

@endsection