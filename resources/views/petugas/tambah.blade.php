@extends('layouts.master')

@section('content')
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title"> Tambah Anggota </h3>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="">Anggota</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah Anggota</li>
          </ol>
        </nav>
      </div>
      <div class="row">
        <div class="col-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <form class="forms-sample">
                <div class="form-group">
                  <label for="exampleInputName1">Name</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Nama">
                </div>
                <div class="form-group">
                    <label for="exampleTextarea1">Alamat</label>
                    <textarea class="form-control" id="exampleTextarea1" rows="4" placeholder="Contoh: Jl. Soedirman No. 9, Sleman, DI Yogyakarta"></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleSelectGender">Jenis Kelamin</label>
                  <select class="form-control" id="exampleSelectGender">
                    <option>Laki-Laki</option>
                    <option>Perempuan</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputCity1">Nomor Handphone</label>
                  <input type="text" class="form-control" id="exampleInputCity1" placeholder="Contoh: 081234567890">
                </div>
                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <button class="btn btn-dark">Cancel</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection