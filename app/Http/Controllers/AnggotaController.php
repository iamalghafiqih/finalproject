<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class AnggotaController extends Controller
{
    public function create()
    {
        return view('anggota.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
                'nama' => 'required',
                'gameplay' => 'required',
                'developer' => 'required',
                'year' => 'required'
            ] );

            DB::table('game')->insert([
                'nama' => $request['nama'],
                'gameplay' => $request['gameplay'],
                'developer'  => $request['developer'],
                'year'  => $request['year']
            ]);

        return redirect('/game');
    }

    public function index()
    {
        $anggota = DB::table('anggota')->get(); 
        // dd($cast);
        return view('anggota.tampil', ['anggota' => $anggota]);

    }

    public function show($id)
    {
        $game = DB::table('game')->where('id', $id)->first();

        return view('game.detail', ['game' => $game]);
    }

    public function edit($id)
    {
        $game = DB::table('game')->where('id', $id)->first();

       return view('game.edit', ['game' => $game]);
    }

    public function update(Request $request, $id)
    {
        DB::table('game')
            ->where('id', $id)
            ->update(
                ['nama' => $request->nama,
                'gameplay' => $request->gameplay,
                'developer' => $request->developer,
                'year' => $request->year]
        );
        return redirect('/game');
    }

    public function destroy($id)
    {
        DB::table('game')->where('id', $id)->delete();

        return redirect('/game');
    }
}
