<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\gameController;
use App\Http\Controllers\AnggotaController;
use App\Http\Controllers\PetugasController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// sudah buat branch

Route::get('/', function () {
    return view('layouts.master');
});

Route::get('/masuk', function () {
    return view('masuk.login');
});

Route::get('/registrasi', function () {
    return view('registrasi.register');
});

//Anggota
Route::get('/anggota/create', [AnggotaController::class, 'create']);

Route::post('/anggota', [AnggotaController::class, 'store']);

Route::get('/anggota', [AnggotaController::class, 'index']);

//petugas
Route::get('/petugas', function () {
    return view('petugas.tampil');
});

Route::get('/petugas/create', [PetugasController::class, 'create']);